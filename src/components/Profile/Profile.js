import React from "react";

import "./Profile.css";

class Profile extends React.Component {
  render() {
    return (
      <div className="profile container">
        <div className="grid">
          <div className="profile__image">
            <img className="avatar" src="..." />
          </div>
          <div className="profile__about">
            <div className="grid">
              <h1 className="profile__name">...</h1>
              <span className="certification">
                <i className="fa fa-check"></i>
                <i className="fa fa-certificate"></i>
              </span>
              <button className="button">Follow</button>
            </div>
            <ul className="stats grid">
              <li>
                <strong>#</strong> posts
              </li>
              <li>
                <strong>#</strong> followers
              </li>
              <li>
                <strong>#</strong> following
              </li>
            </ul>
            <p>
              <strong>...</strong>
              <br />
              ...
            </p>
          </div>
        </div>
        <nav>
          <ul className="options">
            <li className="options__item options__item--selected">
              <i className="fa fa-th"></i> Posts
            </li>
            <li className="options__item">
              <i className="fa fa-tags"></i> Tagged
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Profile;

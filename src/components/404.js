import React from "react";

export default () => (
  <div className="center">
    <h4 style={{ margin: 0 }}>Sorry, this page isn’t available.</h4>
    <p style={{ fontSize: 14 }}>The link you followed may be broken, or the page may have been removed.</p>
  </div>
);

import React from "react";

import "./Header.css";

class Header extends React.Component {
  render() {
    return (
      <header>
        <div className="container">
          <nav className="nav grid grid--center grid--separate">
            <ul className="nav__list grid grid--center logo">
              <li className="nav__item">
                <a href="/">
                  <i className="fa fa-instagram"></i>
                </a>
              </li>
              <hr className="logo__separator" />
              <li className="nav__item">
                <a href="/">
                  <img className="logo__name" src="https://www.dafont.com/forum/attach/orig/7/3/737566.png?1" />
                </a>
              </li>
            </ul>
            <div className="search">
              {/* Search... */}
            </div>
            <ul className="nav__list grid grid--center">
              <li className="nav__item">
                <a className="nav__link nav__link--selected" href="/">
                  Log In
                </a>
              </li>
              <li className="nav__item">
                <a className="nav__link" href="/">
                  Sign Up
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    );
  }
};

export default Header;
